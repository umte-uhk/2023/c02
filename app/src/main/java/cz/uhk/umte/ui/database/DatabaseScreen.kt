package cz.uhk.umte.ui.database

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Button
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import org.koin.androidx.compose.getViewModel

@Composable
fun DatabaseScreen(
    viewModel: DatabaseViewModel = getViewModel()
) {

    val notes = viewModel.notes.collectAsState(emptyList())
    val inputText = remember { mutableStateOf("") }

    Column {
        LazyColumn(
            modifier = Modifier.fillMaxWidth().weight(1f)
        ) {
            items(notes.value) { note ->
                Text(text = note.text,
                modifier = Modifier.padding(16.dp))
            }
        }


        Row {
            OutlinedTextField(
                value = inputText.value,
                onValueChange = {
                    inputText.value = it
                },
                modifier = Modifier.weight(1F)
            )
            Button(onClick = {
                viewModel.createNote(inputText.value)
                inputText.value = ""
            }) {
                Text(text = "Add")
            }
        }
    }
}