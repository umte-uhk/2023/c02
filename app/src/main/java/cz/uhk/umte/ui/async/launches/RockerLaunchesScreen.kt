package cz.uhk.umte.ui.async.launches

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Button
import androidx.compose.material.Card
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import cz.uhk.umte.base.State
import androidx.compose.runtime.State as ComposeState
import cz.uhk.umte.data.model.response.RocketLaunchResponse
import cz.uhk.umte.ui.basic.lazylist.generateHumans
import org.koin.androidx.compose.getViewModel

@Composable
fun RockerLaunchesScreen(
    onNavigateDetail: (rocketId: String) -> Unit,
    viewModel: RocketLaunchesViewModel = getViewModel()
) {

    val launches = viewModel.successRocketLaunches.collectAsState()
    val state = viewModel.state.collectAsState()

    when (val result = state.value) {
        State.None, State.Loading -> {
            CircularProgressIndicator()
        }
        is State.Failure -> {
            Column {
                Text(text = "Chyba - ${result.throwable.localizedMessage}")
                Button(onClick = { viewModel.fetchSuccessRocketLaunches() }) {
                    Text("Zkusit znovu")
                }
            }

        }
        is State.Success -> {
            RocketLaunchesView(launches, onNavigateDetail)
        }
    }

}
@Composable
fun RocketLaunchesView(
    rocketLaunches: ComposeState<List<RocketLaunchResponse>>,
    onNavigateDetail: (rocketId: String) -> Unit,
) {
    LazyColumn(
        modifier = Modifier.fillMaxSize()
    ) {
        items(rocketLaunches.value) { rocketLaunch ->
            Box(
                modifier = Modifier
                    .padding(16.dp)
                    .clickable {
                        // akce po kliku
                        onNavigateDetail.invoke(rocketLaunch.rocket.id)
                    }
            ) {
                Card(
                    modifier = Modifier.fillMaxWidth()
                ) {
                    Row(
                        modifier = Modifier
                            .padding(16.dp)
                    ) {
                        Text(text = rocketLaunch.missionName)
                        Spacer(modifier = Modifier.width(4.dp))
                        Text(text = "Flight num: ${rocketLaunch.flightNumber}")
                    }
                }
            }
        }
    }
}