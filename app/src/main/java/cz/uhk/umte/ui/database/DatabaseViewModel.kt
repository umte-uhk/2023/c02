package cz.uhk.umte.ui.database

import cz.uhk.umte.base.BaseViewModel
import cz.uhk.umte.data.db.dao.NoteDao
import cz.uhk.umte.data.db.entities.NoteEntity

class DatabaseViewModel(
    private val noteDao: NoteDao
): BaseViewModel() {

    val notes = noteDao.selectAll()

    fun createNote(text: String) {
        launch {
            noteDao.insertOrUpdate(NoteEntity(text = text))
        }
    }

}