package cz.uhk.umte.di

import androidx.room.Room
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import cz.uhk.umte.data.db.AppDatabase
import cz.uhk.umte.data.remote.service.SpaceXAPIService
import cz.uhk.umte.data.repository.SpaceXRepository
import cz.uhk.umte.ui.async.launches.RocketLaunchesViewModel
import cz.uhk.umte.ui.async.rocketDetail.RocketDetailViewModel
import cz.uhk.umte.ui.database.DatabaseViewModel
import kotlinx.serialization.json.Json
import okhttp3.MediaType
import okhttp3.OkHttpClient
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit

val databaseModule = module {
    single {
        Room.databaseBuilder(
            context = androidApplication(),
            klass = AppDatabase::class.java,
            name = AppDatabase.Name
        ).build()
    }

    single { get<AppDatabase>().noteDao() }
}

val uiModule = module {
    viewModel { RocketLaunchesViewModel(get()) }
    viewModel { (rocketId: String) -> RocketDetailViewModel(rocketId, get()) }
    viewModel { DatabaseViewModel(get()) }
}

val dataModule = module {
    single { createSpaceXService() }
    single { SpaceXRepository(get()) }
}

private val json = Json {
    ignoreUnknownKeys = true
}

fun createSpaceXService() = createRetrofit().create(SpaceXAPIService::class.java)

fun createRetrofit() = Retrofit.Builder().apply {
    client(OkHttpClient.Builder().build())
    baseUrl("https://api.spacexdata.com/v3/")
    addConverterFactory(json.asConverterFactory(MediaType.get("application/json")))
}.build()