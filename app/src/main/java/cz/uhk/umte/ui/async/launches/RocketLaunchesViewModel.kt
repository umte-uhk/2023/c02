package cz.uhk.umte.ui.async.launches

import cz.uhk.umte.base.BaseViewModel
import cz.uhk.umte.data.model.response.RocketLaunchResponse
import cz.uhk.umte.data.repository.SpaceXRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

class RocketLaunchesViewModel(
    private val spaceXRepository: SpaceXRepository
): BaseViewModel() {

    private val _successRocketLaunches = MutableStateFlow<List<RocketLaunchResponse>>(emptyList())
    val successRocketLaunches = _successRocketLaunches.asStateFlow()

    init {
        fetchSuccessRocketLaunches()
    }

    fun fetchSuccessRocketLaunches() = launch(
        block = {
            spaceXRepository.fetchAllSuccessfulLaunches().also {
                _successRocketLaunches.emit(it)
            }

            // Ekvivalent kodu vyse
//            val data = spaceXRepository.fetchAllSuccessfulLaunches()
//            _successRocketLaunches.emit(data)
//            return@launch data
        }
    )

}