package cz.uhk.umte.ui.async.rocketDetail

import cz.uhk.umte.base.BaseViewModel
import cz.uhk.umte.data.model.response.RocketDetailResponse
import cz.uhk.umte.data.repository.SpaceXRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

class RocketDetailViewModel(
    private val rocketId: String,
    private val spaceXRepository: SpaceXRepository
): BaseViewModel() {

    private val _rocketDetailState = MutableStateFlow<RocketDetailResponse?>(null)
    val rocketDetailState = _rocketDetailState.asStateFlow()

    init {
        fetchRocketDetail()
    }

    fun fetchRocketDetail() = launch(
        block = {
            spaceXRepository.fetchRocketDetail(rocketId).also { rocketDetail ->
                _rocketDetailState.emit(rocketDetail)
            }
        }
    )


}