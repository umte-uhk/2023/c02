package cz.uhk.umte.data.repository

import cz.uhk.umte.data.model.response.RocketDetailResponse
import cz.uhk.umte.data.remote.service.SpaceXAPIService

class SpaceXRepository(
    private val spaceXAPIService: SpaceXAPIService
): BaseRepository() {

    suspend fun fetchAllSuccessfulLaunches() =
        spaceXAPIService.fetchAllLaunches(wasLaunchSuccessful = true)

    suspend fun fetchRocketDetail(rocketId: String): RocketDetailResponse? {
        return spaceXAPIService.fetchRocketDetail(rocketId)
    }

}