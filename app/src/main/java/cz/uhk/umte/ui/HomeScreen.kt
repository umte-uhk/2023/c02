package cz.uhk.umte.ui

import android.content.Intent
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import cz.uhk.umte.R
import cz.uhk.umte.ui.basic.dialog.DialogActivity
import cz.uhk.umte.ui.basic.form.FormActivity
import cz.uhk.umte.ui.basic.lazylist.LazyListActivity


@Composable
fun HomeScreen(
    parentController: NavHostController
) {
    val context = LocalContext.current

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp)
            .background(Color.Cyan)
            .padding(16.dp)
            .background(Color.Red),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(text = "View 1")
        Text(text = "View 2")
        Text(text = "View 3")

        Button(onClick = {
            context.startActivity(Intent(context, FormActivity::class.java))
        }) {
            Icon(Icons.Default.Home, contentDescription = "")
            Text(text = "Formulář")
        }

        Button(onClick = {
            context.startActivity(Intent(context, LazyListActivity::class.java))
        }) {
            Icon(Icons.Default.List, contentDescription = "")
            Text(text = "Lazy list")
        }

        Button(onClick = {
            context.startActivity(Intent(context, DialogActivity::class.java))
        }) {
            Icon(Icons.Default.List, contentDescription = "")
            Text(text = "Dialog")
        }

        Button(onClick = {
            parentController.navigateRocketLaunchesScreen()
        }) {
            Icon(Icons.Default.Call, contentDescription = "")
            Text(text = "Async operations")
        }

        Button(onClick = {
             parentController.navigateDatabaseScreen()
        }) {
            Icon(Icons.Default.DateRange, contentDescription = "")
            Text(text = "Databaze")
        }

        Button(onClick = {
            parentController.navigateIntentsScreen()
        }) {
            Icon(Icons.Default.ArrowForward, contentDescription = "")
            Text(text = "Intents")
        }

        Button(onClick = {
            parentController.navigateNotificationsScreen()
        }) {
            Icon(Icons.Default.ArrowForward, contentDescription = "")
            Text(text = "Notifikace")
        }
    }
}