package cz.uhk.umte.ui.basic.lazylist

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp

val names = listOf<String>("Petr", "Jan", "Honza")
val surnames = listOf<String>("Novák", "Žižka", "Krocan")

@Composable
fun LazyListScreen() {
    LazyColumn(
        modifier = Modifier.fillMaxSize()
    ) {
        items(generateHumans(100)) { human ->
            Box(
                modifier = Modifier.padding(16.dp)
            ) {
                Card(
                    modifier = Modifier.fillMaxWidth()
                ) {
                    Row(
                        modifier = Modifier
                            .background(Color.Green)
                            .padding(16.dp)
                    ) {
                        Text(text = human.name)
                        Spacer(modifier = Modifier.width(4.dp))
                        Text(text = human.surname)
                    }
                }
            }
        }
    }
}

data class Human(
    val name: String,
    val surname: String
)

fun generateHumans(count: Int): List<Human> {
    val list = mutableListOf<Human>()

    repeat(count) { index ->
        list.add(Human(names.random(), surnames.random()))
    }

    return list
}


//class Human {
//
//    var name: String = ""
//        get() {
//            return field
//        }
//        private set(value) {
//            field = value
//        }
//
//    val surname: String = ""
//
//}