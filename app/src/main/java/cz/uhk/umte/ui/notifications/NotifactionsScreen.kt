package cz.uhk.umte.ui.notifications

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.widget.Toast
import androidx.activity.compose.ManagedActivityResultLauncher
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.layout.Column
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.platform.LocalContext
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import cz.uhk.umte.R

@Composable
fun NotifictionsScreen() {

    val context = LocalContext.current

    val permissionGranted = remember { mutableStateOf(isNotificationGranted(context)) }

    val launcher = rememberLauncherForActivityResult(
        contract = ActivityResultContracts.RequestPermission(),
        onResult = { isGranted ->
            if (isGranted) {
                Toast.makeText(context, "Povoleni udeleno", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(context, "Povoleni NEUDELENO", Toast.LENGTH_SHORT).show()
            }

            permissionGranted.value = isGranted
        }
    )

    Column {

        Button(onClick = { requestNotificationPermission(context, launcher) }) {
            Text(text = "Získat povolení")
        }

        if (permissionGranted.value) {
            Text(text = "Povoleni udeleno")
        } else {
            Text(text = "Povoleni neudeleno")
        }

        Button(onClick = { sendNotification(context) }) {
            Text(text = "Odeslat notifickaci")
        }

    }

}

private fun sendNotification(context: Context) {
    val builder = NotificationCompat.Builder(context, "general")
        .setContentTitle("Ahoj svete")
        .setContentText("Popisek notifikace")
        .setSmallIcon(R.drawable.ic_launcher_foreground)
        .setPriority(NotificationCompat.PRIORITY_HIGH)

    NotificationManagerCompat.from(context).notify(1, builder.build())
}

private fun isNotificationGranted(context: Context): Boolean {
    return ActivityCompat.checkSelfPermission(
        context,
        Manifest.permission.POST_NOTIFICATIONS
    ) == PackageManager.PERMISSION_GRANTED
}

private fun requestNotificationPermission(
    context: Context,
    launcher: ManagedActivityResultLauncher<String, Boolean>
) {
    if (isNotificationGranted(context)) return

    launcher.launch(Manifest.permission.POST_NOTIFICATIONS)
}