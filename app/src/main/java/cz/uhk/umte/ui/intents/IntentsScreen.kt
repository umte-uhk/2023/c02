package cz.uhk.umte.ui.intents

import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.compose.foundation.layout.Column
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalContext

@Composable
fun IntentsScreen() {
    val context = LocalContext.current

    Column {
        Button(onClick = { openEmailClient(context) }) {
            Text("e-mail")
        }

        Button(onClick = { openMap(context) }) {
            Text("navigace")
        }
    }

}

private fun openEmailClient(context: Context) {
    val email = "filip.obornik@uhk.cz"
    val subject = "Predmet emailu"
    val body = "Ahoj emaile."

    // Na cviceni se nepredvyplnoval email, nutna uprava byla pridat
    // dalsi vstupni parametr do konstruktoru Intentu - konkretne Uri.parse("mailto:")
    val intent = Intent(Intent.ACTION_SEND, Uri.parse("mailto:")).apply {
        type = "text/html"
        putExtra(Intent.EXTRA_EMAIL, email)
        putExtra(Intent.EXTRA_SUBJECT, subject)
        putExtra(Intent.EXTRA_TEXT, body)
    }

    context.startActivity(intent)
}

private fun openMap(context: Context) {
    val intent = Intent(Intent.ACTION_VIEW, Uri.parse("geo:?q=50.204176,15.829265"))
    context.startActivity(intent)
}