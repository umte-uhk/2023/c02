package cz.uhk.umte.ui.basic.lazylist

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent

class LazyListActivity: ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            LazyListScreen()



        }
    }

    // create function to generate humans with name and surname from lists of names and surnames. Use repeat function to generate list of humans. Use random function to get random name and surname from list

}