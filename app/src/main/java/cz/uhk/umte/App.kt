package cz.uhk.umte

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import androidx.core.app.NotificationManagerCompat
import cz.uhk.umte.di.dataModule
import cz.uhk.umte.di.databaseModule
import cz.uhk.umte.di.uiModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class App: Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(applicationContext)
            modules(listOf(dataModule, uiModule, databaseModule))
        }

        createNotificationChannel()
    }

    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = "Obecna upozorneni"
            val description = "Notifikace na vse"
            val importance = NotificationManager.IMPORTANCE_HIGH
            val channel = NotificationChannel("general", name, importance).apply {
                this.description = description
            }

            val notificationmanager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationmanager.createNotificationChannel(channel)
        }
    }

}