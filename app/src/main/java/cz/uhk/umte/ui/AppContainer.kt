package cz.uhk.umte.ui

import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.navArgument
import cz.uhk.umte.ui.async.launches.RockerLaunchesScreen
import cz.uhk.umte.ui.async.rocketDetail.RocketDetailScreen
import cz.uhk.umte.ui.database.DatabaseScreen
import cz.uhk.umte.ui.intents.IntentsScreen
import cz.uhk.umte.ui.notifications.NotifictionsScreen

@Composable
fun AppContainer(
    controller: NavHostController
) {
    // HomeScreen()

    NavHost(navController = controller, startDestination = DestinationHome) {
        composable(
            route = DestinationHome
        ) {
            HomeScreen(controller)
        }

        composable(
            route = DestinationRocketLaunches
        ) {
            RockerLaunchesScreen(
                onNavigateDetail = { rocketId ->
                    controller.navigateRocketDetailScreen(rocketId)
                }
            )
        }

        composable(
            route = DestinationRocketDetail,
            arguments = listOf(navArgument(ArgRocketId) { type = NavType.StringType })
        ) { backStackEntry ->
            RocketDetailScreen(backStackEntry.arguments?.getString(ArgRocketId))
        }

        composable(
            route = DestinationDatabaseScreen,
        ) {
            DatabaseScreen()
        }

        composable(
            route = DestinationIntentsScreen
        ) {
            IntentsScreen()
        }

        composable(
            route = DestinationNotificationsScreen
        ) {
            NotifictionsScreen()
        }


    }

}

fun NavHostController.navigateRocketLaunchesScreen() {
    navigate(DestinationRocketLaunches)
}

fun NavHostController.navigateNotificationsScreen() {
    navigate(DestinationNotificationsScreen)
}

fun NavHostController.navigateRocketDetailScreen(rocketId: String) {
    navigate(DestinationRocketDetail.replaceArg(ArgRocketId, rocketId))
    // TODO vyresit null
}

fun NavHostController.navigateDatabaseScreen() {
    navigate(DestinationDatabaseScreen)
}

fun NavHostController.navigateIntentsScreen() {
    navigate(DestinationIntentsScreen)
}


private fun String.replaceArg(argName: String, newString: String) =
    replace("{$argName}", newString)

private const val ArgRocketId = "argRocketId"

private const val DestinationHome = "home"
private const val DestinationRocketLaunches = "rocket-launches"
private const val DestinationRocketDetail = "rocket/{$ArgRocketId}"
private const val DestinationDatabaseScreen = "database"
private const val DestinationIntentsScreen = "intents"
private const val DestinationNotificationsScreen = "notifications"

