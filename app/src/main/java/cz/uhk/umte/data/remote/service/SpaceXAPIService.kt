package cz.uhk.umte.data.remote.service

import cz.uhk.umte.data.model.response.RocketDetailResponse
import cz.uhk.umte.data.model.response.RocketLaunchResponse
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface SpaceXAPIService {


    // Jak pouzivat ostatni metody najdete v dokumentaci knihovny Retrofit
    // Retrofit kotlin guide

    // https://api.spacexdata.com/v3/launches?launch_successful=true GET

    @GET("launches")
    suspend fun fetchAllLaunches(
        @Query("launch_successful") wasLaunchSuccessful: Boolean
    ): List<RocketLaunchResponse>

    // https://api.spacexdata.com/v3/rocket/{rocketId} GET

    @GET("rockets/{rocketId}")
    suspend fun fetchRocketDetail(
        @Path("rocketId") rocketId: String
    ): RocketDetailResponse?

}