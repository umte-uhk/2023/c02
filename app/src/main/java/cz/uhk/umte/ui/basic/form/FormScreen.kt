package cz.uhk.umte.ui.basic.form

import android.app.Activity
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Home
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.Alignment
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp
import cz.uhk.umte.R
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import cz.uhk.umte.data.enum.Gender
import cz.uhk.umte.ui.views.RadioText

@Composable
fun FormScreen() {
    val context = LocalContext.current
    val inputName = remember { mutableStateOf("") }
    val inputAge = remember { mutableStateOf("") }

    val genderSelection = remember { mutableStateOf("") }

    Scaffold(topBar = {
        TopAppBar(
            title = {
                Text(text = context.getString(R.string.form_screen_title))
            },
            navigationIcon = {
                IconButton(onClick = { (context as Activity).finish() }) {
                    Icon(Icons.Default.ArrowBack, contentDescription = "")
                }
            },
            elevation = 8.dp,
            backgroundColor = Color.Blue,
            contentColor = Color.White
        )
    }
    ) {
        Column(
            modifier = Modifier
                .padding(16.dp)
                .fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Row(
                verticalAlignment = Alignment.CenterVertically
            ) {
                Icon(imageVector = Icons.Default.Home, contentDescription = "")
                Image(painter = painterResource(R.drawable.ic_launcher_foreground), contentDescription = "")
                Text(
                    text = context.getString(R.string.form_screen_title), style = TextStyle(
                        fontWeight = FontWeight.Normal,
                        fontSize = 24.sp,
                        letterSpacing = 0.sp
                    )
                )
            }

            OutlinedTextField(
                modifier = Modifier.fillMaxWidth(),
                value = inputName.value,
                onValueChange = { text ->
                    inputName.value = text
                },
                label = {
                    Text(text = context.getString(R.string.form_screen_input_name))
                }
            )

            Spacer(modifier = Modifier.height(20.dp))

            OutlinedTextField(
                value = inputAge.value,
                onValueChange = { text ->
                    inputAge.value = text
                },
                label = {
                    Text(text = context.getString(R.string.form_screen_input_age))
                },
                keyboardOptions = KeyboardOptions(
                    keyboardType = KeyboardType.Number
                )
            )

            Spacer(modifier = Modifier.weight(0.5f))

            Row {
                RadioText(label = context.getString(Gender.Man.nameRes), genderSelection)
                RadioText(label = context.getString(Gender.Woman.nameRes), genderSelection)
                RadioText(label = context.getString(Gender.Undefined.nameRes), genderSelection)
            }

            Spacer(modifier = Modifier.weight(1f))

            Button(onClick = { (context as Activity).finish() }) {
                Text(text = context.getString(R.string.form_screen_btn_finish))
            }
        }
        it
    }
}